const User = require('../model/user')
const Op = require('sequelize').Op

const loginIn = async (ctx) => {
    const user = ctx.request.body
    const data = await User.findOne({
        where: {
            name: {
                [Op.eq]: `${user.userName}`
            },
            password: user.password
        }
    })
    ctx.body = {
        code: data ? 1000 : 1003,
        data,
        desc: data ? '登陆成功' : '账号或密码错误'
    }
}

const register = async (ctx) => {
    const { username,password} = ctx.request.body
    const data = await User.create({
        username,
        password
    })
    ctx.body = {
        code: data ? 200 : 1003,
        data,
        desc: data ? '注册成功' : '账号或密码错误'
    }
}

module.exports = {
    loginIn,
    register
}