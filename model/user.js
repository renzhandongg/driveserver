const sequelize = require('../sequelize ')
const Sequelize = require('sequelize')
const moment = require('moment')

const user = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    username: {
        type: Sequelize.STRING,
        unique: {
            msg: '已添加'
        }
    },
    password: {
        type: Sequelize.INTEGER(6)
    },
    createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            get() {
                return moment(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm')
            }
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            get() {
                return moment(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm')
            }
        }
}, {
    freezeTableName: true
})

module.exports = user